Introdução

Este é um aplicativo React Native que utiliza várias funcionalidades, como rastreamento de localização, mapeamento e exibição de pontos de interesse (farmácias). Ele fornece funcionalidades para rastrear a localização do usuário, exibi-la no mapa e desenhar uma polilinha representando o histórico de movimento do usuário. Além disso, ele exibe farmácias próximas como marcadores no mapa.
Pré-requisitos

    Node.js instalado em sua máquina.
    Expo CLI instalado globalmente.
    Chave da API do Google Maps.

Instalação

    Clone este repositório em sua máquina local.
    Navegue até o diretório do projeto.
    Execute npm install para instalar as dependências.
    Substitua a variável YOUR_API_KEY em App.js pela sua Chave da API do Google Maps.
    Execute expo start para iniciar o servidor de desenvolvimento.

Uso

    Assim que o aplicativo estiver em execução no seu dispositivo ou emulador, você verá um mapa exibindo sua localização atual (se a permissão for concedida).
    Toque no botão "Começar rastreamento" para iniciar o rastreamento de sua localização.
    Sua localização será continuamente atualizada no mapa, e uma polilinha vermelha indicará seu histórico de movimento.
    Farmácias próximas serão exibidas como marcadores no mapa.

Componentes e Bibliotecas Utilizadas

    React Native
    React Native Maps: Usado para exibir mapas e marcadores.
    Expo Location: Usado para acessar serviços de localização.
    MapViewDirections: Usado para desenhar direções no mapa.

Visão Geral do Código

    App.js: Este arquivo contém a lógica principal do aplicativo. Ele configura a visualização do mapa, trata o rastreamento de localização e exibe farmácias próximas como marcadores.
    styles.js: Contém estilos para os componentes.

Capturas de Tela


Captura de Tela 1: Mapa exibindo a localização atual do usuário e farmácias próximas.
Licença

Este projeto está licenciado sob a Licença MIT - consulte o arquivo LICENSE para obter detalhes.