import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  TextInput,
  View,
  StatusBar,
} from 'react-native';
import MapView, { Marker, Polyline } from 'react-native-maps';
import * as Location from 'expo-location';

export default function App() {
  const [localizacao, setLocalizacao] = useState(null);
  const [historico, setHistorico] = useState([]);
  const [dataInicio, setDataInicio] = useState(null);
  const [dataFim, setDataFim] = useState(null);
  const [rastreando, setRastreando] = useState(false);
  const [zoom, setZoom] = useState(0);

  const farmacias = [
    { coordenada: { latitude: -21.76183889417198, longitude: -43.34936069454236 }, titulo: "Drogaria Souza" },
    { coordenada: { latitude: -21.76135710777179, longitude: -43.349365205462114 }, titulo: "Drogaria Pacheco" },
    { coordenada: { latitude: -21.761977145616456, longitude: -43.34839084677622 }, titulo: "Manipulação Arcanjo Miguel" },
    { coordenada: { latitude: -21.761126687610883, longitude: -43.34871112207805 }, titulo: "Drogaria Souza" },
    { coordenada: { latitude: -21.760984245875893, longitude: -43.34904493013911 }, titulo: "Drogaria Araujo" },
    { coordenada: { latitude: -21.760623951436184, longitude: -43.34964939338789 }, titulo: "Drogaria Serve Mais" },
    { coordenada: { latitude: -21.76031726038667, longitude: -43.34961678842428 }, titulo: "Farmacia São Mateus" },
    { coordenada: { latitude: -21.759261258187006, longitude: -43.351200468529036 }, titulo: "Drogaria Mais" },
    { coordenada: { latitude: -21.759201368051922, longitude: -43.35041906597735 }, titulo: "Drogaria JR" },
    { coordenada: { latitude: -21.758704631265445, longitude: -43.348621081439056 }, titulo: "Drogaria Araujo" },
    { coordenada: { latitude: -21.666869290001827, longitude: -43.305061649284795 }, titulo: "Drogaria Barateira" }
  ];

  const mudarZoom = (local) => {
    const { width } = Dimensions.get('screen');
    const nivelZoom =
      Math.log2(360 * (width / 256 / local.longitudeDelta)) + 1;
    setZoom(nivelZoom);
  };

  const isValidDate = (text) => {
    const regex = /^\d{4}-\d{2}-\d{2}$/; // Regex para verificar o formato YYYY-MM-DD
    if (!regex.test(text)) {
      // Se o formato não estiver correto, a data não é válida
      return false;
    }
    const date = new Date(text);
    if (isNaN(date.getTime())) {
      // Se a data não puder ser convertida para um objeto de data válida, não é válida
      return false;
    }
    return true;
  };
  

  useEffect(() => {
    let interval;

    const rastrear = async () => {
      const { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        console.log('Erro');
        return;
      }

      interval = setInterval(async () => {
        const novaLocalizacao = await Location.getCurrentPositionAsync({});
        setLocalizacao(novaLocalizacao);
        const local = {
          latitude: novaLocalizacao.coords.latitude,
          longitude: novaLocalizacao.coords.longitude,
          timestamp: novaLocalizacao.timestamp // Adicionando o timestamp ao objeto local
        };
        mudarZoom(local);
        setHistorico((historicoAnterior) => [
          ...historicoAnterior,
          local,
        ]);
      }, 3000);
    };

    const pararRastreamento = () => {
      clearInterval(interval);
    };

    if (rastreando) {
      rastrear();
    } else {
      pararRastreamento();
    }

    return () => {
      pararRastreamento();
    };
  }, [rastreando]);

  const filtrarLocalizacoes = () => {
    if (!dataInicio || !dataFim) {
      return;
    }
  
    const inicio = new Date(dataInicio).getTime();
    const fim = new Date(dataFim).getTime();
  
    const localizacoesFiltradas = historico.filter((localizacao) => {
      const timestamp = new Date(localizacao.timestamp).getTime();
      return timestamp >= inicio && timestamp <= fim;
    });
  
    setHistorico(localizacoesFiltradas);
  };
  

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar></StatusBar>
    <View style={styles.filtroContainer}>
      <TextInput
        style={styles.input}
        placeholder="Data de início AAAA-MM-DD"
        onChangeText={(text) => {
          if (isValidDate(text)) {
            setDataInicio(text);
          }
        }}
      />

      <TextInput
        style={styles.input}
        placeholder="Data de fim AAAA-MM-DD"
        onChangeText={(text) => {
          // Validar se o texto inserido é uma data válida
          if (isValidDate(text)) {
            setDataFim(text);
          }
        }}
      />

        <TouchableOpacity style={styles.botao} onPress={filtrarLocalizacoes}>
          <Text style={styles.textoBotao}>Filtrar</Text>
        </TouchableOpacity>
      </View>
      <MapView
        style={styles.map}
        onRegionChangeComplete={mudarZoom}
        initialRegion={
          localizacao
            ? {
                latitude: localizacao.coords.latitude,
                longitude: localizacao.coords.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }
            : null
        }>
        {localizacao ? (
          <Marker
            
            coordinate={{
              latitude: localizacao.coords.latitude,
              longitude: localizacao.coords.longitude,
            }}
            title="Minha localização"
            description="Eu estou aqui"
            pinColor='#00945A'
            
            
          />
        ) : null}
        {farmacias.map((farmacia, index) => (
          <Marker
            key={index}
            coordinate={{
              latitude: farmacia.coordenada.latitude,
              longitude: farmacia.coordenada.longitude,
            }}
            title={farmacia.titulo}
          />
        ))}
        <Polyline
          coordinates={historico.map((localizacao) => ({
            latitude: localizacao.latitude,
            longitude: localizacao.longitude,
          }))}
          strokeColor="red"
          strokeWidth={12}
        />
      </MapView>



      <TouchableOpacity
        style={[styles.botao, rastreando ? styles.botaoDesligado : styles.botaoLigado]}
        onPress={() => setRastreando((estadoAnterior) => !estadoAnterior)}>
        <Text style={styles.textoBotao}>
          {rastreando ? 'Desligar Rastreamento' : 'Ligar Rastreamento'}
        </Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    flex: 1,
    width: '100%',
  },
  botao: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 10,
    elevation: 2,
    marginVertical: 5,
    
  },
  botaoLigado: {
    backgroundColor: 'green',
  },
  botaoDesligado: {
    backgroundColor: 'red',
  },
  textoBotao: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
  },
  filtroContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingTop: 5,
    paddingBottom: 5,
    borderBottomColor:'#000',
    borderBottomWidth: 1,
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    padding: 10,
    flex: 1,
    marginRight: 10,
  },
});
